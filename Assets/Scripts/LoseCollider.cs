﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoseCollider : MonoBehaviour {

	public LevelManager levelManager;

	void OnTriggerEnter2D (Collider2D trigger) {
		print ("Trigger");
		SceneManager.LoadScene("Lose");

	}

	void OnCollisionenter2D (Collision2D collision) {
		print ("Collision");
	}
}
